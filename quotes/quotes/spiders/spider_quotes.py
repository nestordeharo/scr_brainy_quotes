# -*- coding: utf-8 -*-
import scrapy
import urlparse
from quotes.items import QuotesItem

class SpiderQuotesSpider(scrapy.Spider):
    name = "spider_quotes"
    allowed_domains = ["brainyquote.com"]
    start_urls = ['http://brainyquote.com/']

    def parse(self, response):
    	for individual_value in response.xpath('//div[contains(@class, "bqLn")]/a[contains(@href,"topic")]'):
    		text = individual_value.xpath('./text()').extract_first()
    		
    		## Create a full link of the topic
    		link = individual_value.xpath('./@href').extract_first()
    		link = urlparse.urljoin(response.url, link)
    		
    		## Generate a item class and export to the processing
    		i = QuotesItem()
    		i['text'] = text
    		i['link'] = link
    		yield i
        	
